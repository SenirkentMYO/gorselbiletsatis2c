﻿namespace BiletSatis
{
    partial class Koltuk
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.K1 = new System.Windows.Forms.Button();
            this.k2 = new System.Windows.Forms.Button();
            this.k3 = new System.Windows.Forms.Button();
            this.k4 = new System.Windows.Forms.Button();
            this.k5 = new System.Windows.Forms.Button();
            this.k6 = new System.Windows.Forms.Button();
            this.k7 = new System.Windows.Forms.Button();
            this.k8 = new System.Windows.Forms.Button();
            this.k9 = new System.Windows.Forms.Button();
            this.k10 = new System.Windows.Forms.Button();
            this.k11 = new System.Windows.Forms.Button();
            this.k12 = new System.Windows.Forms.Button();
            this.k13 = new System.Windows.Forms.Button();
            this.k14 = new System.Windows.Forms.Button();
            this.k15 = new System.Windows.Forms.Button();
            this.k16 = new System.Windows.Forms.Button();
            this.k17 = new System.Windows.Forms.Button();
            this.k18 = new System.Windows.Forms.Button();
            this.k19 = new System.Windows.Forms.Button();
            this.k20 = new System.Windows.Forms.Button();
            this.k21 = new System.Windows.Forms.Button();
            this.k22 = new System.Windows.Forms.Button();
            this.ksofor = new System.Windows.Forms.Button();
            this.kmuavin = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bal = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // K1
            // 
            this.K1.Location = new System.Drawing.Point(215, 82);
            this.K1.Name = "K1";
            this.K1.Size = new System.Drawing.Size(75, 23);
            this.K1.TabIndex = 0;
            this.K1.Text = "1.Koltuk";
            this.K1.UseVisualStyleBackColor = true;
            this.K1.Click += new System.EventHandler(this.K1_Click);
            // 
            // k2
            // 
            this.k2.Location = new System.Drawing.Point(306, 82);
            this.k2.Name = "k2";
            this.k2.Size = new System.Drawing.Size(75, 23);
            this.k2.TabIndex = 1;
            this.k2.Text = "2.Koltuk";
            this.k2.UseVisualStyleBackColor = true;
            this.k2.Click += new System.EventHandler(this.k2_Click);
            // 
            // k3
            // 
            this.k3.Location = new System.Drawing.Point(215, 121);
            this.k3.Name = "k3";
            this.k3.Size = new System.Drawing.Size(75, 23);
            this.k3.TabIndex = 2;
            this.k3.Text = "3.Koltuk";
            this.k3.UseVisualStyleBackColor = true;
            this.k3.Click += new System.EventHandler(this.k3_Click);
            // 
            // k4
            // 
            this.k4.Location = new System.Drawing.Point(306, 121);
            this.k4.Name = "k4";
            this.k4.Size = new System.Drawing.Size(75, 23);
            this.k4.TabIndex = 3;
            this.k4.Text = "4.Koltuk";
            this.k4.UseVisualStyleBackColor = true;
            this.k4.Click += new System.EventHandler(this.k4_Click);
            // 
            // k5
            // 
            this.k5.Location = new System.Drawing.Point(215, 160);
            this.k5.Name = "k5";
            this.k5.Size = new System.Drawing.Size(75, 23);
            this.k5.TabIndex = 4;
            this.k5.Text = "5.Koltuk";
            this.k5.UseVisualStyleBackColor = true;
            this.k5.Click += new System.EventHandler(this.k5_Click);
            // 
            // k6
            // 
            this.k6.Location = new System.Drawing.Point(306, 160);
            this.k6.Name = "k6";
            this.k6.Size = new System.Drawing.Size(75, 23);
            this.k6.TabIndex = 5;
            this.k6.Text = "6.Koltuk";
            this.k6.UseVisualStyleBackColor = true;
            this.k6.Click += new System.EventHandler(this.k6_Click);
            // 
            // k7
            // 
            this.k7.Location = new System.Drawing.Point(215, 201);
            this.k7.Name = "k7";
            this.k7.Size = new System.Drawing.Size(75, 23);
            this.k7.TabIndex = 6;
            this.k7.Text = "7.Koltuk";
            this.k7.UseVisualStyleBackColor = true;
            this.k7.Click += new System.EventHandler(this.k7_Click);
            // 
            // k8
            // 
            this.k8.Location = new System.Drawing.Point(306, 201);
            this.k8.Name = "k8";
            this.k8.Size = new System.Drawing.Size(75, 23);
            this.k8.TabIndex = 7;
            this.k8.Text = "8.Koltuk";
            this.k8.UseVisualStyleBackColor = true;
            this.k8.Click += new System.EventHandler(this.k8_Click);
            // 
            // k9
            // 
            this.k9.Location = new System.Drawing.Point(215, 244);
            this.k9.Name = "k9";
            this.k9.Size = new System.Drawing.Size(75, 23);
            this.k9.TabIndex = 8;
            this.k9.Text = "9.Koltuk";
            this.k9.UseVisualStyleBackColor = true;
            this.k9.Click += new System.EventHandler(this.k9_Click);
            // 
            // k10
            // 
            this.k10.Location = new System.Drawing.Point(306, 244);
            this.k10.Name = "k10";
            this.k10.Size = new System.Drawing.Size(75, 23);
            this.k10.TabIndex = 9;
            this.k10.Text = "10.Koltuk";
            this.k10.UseVisualStyleBackColor = true;
            this.k10.Click += new System.EventHandler(this.k10_Click);
            // 
            // k11
            // 
            this.k11.Location = new System.Drawing.Point(215, 288);
            this.k11.Name = "k11";
            this.k11.Size = new System.Drawing.Size(75, 23);
            this.k11.TabIndex = 10;
            this.k11.Text = "11.Koltuk";
            this.k11.UseVisualStyleBackColor = true;
            this.k11.Click += new System.EventHandler(this.k11_Click);
            // 
            // k12
            // 
            this.k12.Location = new System.Drawing.Point(306, 288);
            this.k12.Name = "k12";
            this.k12.Size = new System.Drawing.Size(75, 23);
            this.k12.TabIndex = 11;
            this.k12.Text = "12.Koltuk";
            this.k12.UseVisualStyleBackColor = true;
            this.k12.Click += new System.EventHandler(this.k12_Click);
            // 
            // k13
            // 
            this.k13.Location = new System.Drawing.Point(451, 82);
            this.k13.Name = "k13";
            this.k13.Size = new System.Drawing.Size(75, 23);
            this.k13.TabIndex = 12;
            this.k13.Text = "13.Koltuk";
            this.k13.UseVisualStyleBackColor = true;
            this.k13.Click += new System.EventHandler(this.k13_Click);
            // 
            // k14
            // 
            this.k14.Location = new System.Drawing.Point(543, 82);
            this.k14.Name = "k14";
            this.k14.Size = new System.Drawing.Size(75, 23);
            this.k14.TabIndex = 13;
            this.k14.Text = "14.Koltuk";
            this.k14.UseVisualStyleBackColor = true;
            this.k14.Click += new System.EventHandler(this.k14_Click);
            // 
            // k15
            // 
            this.k15.Location = new System.Drawing.Point(451, 121);
            this.k15.Name = "k15";
            this.k15.Size = new System.Drawing.Size(75, 23);
            this.k15.TabIndex = 14;
            this.k15.Text = "15.Koltuk";
            this.k15.UseVisualStyleBackColor = true;
            this.k15.Click += new System.EventHandler(this.k15_Click);
            // 
            // k16
            // 
            this.k16.Location = new System.Drawing.Point(543, 121);
            this.k16.Name = "k16";
            this.k16.Size = new System.Drawing.Size(75, 23);
            this.k16.TabIndex = 15;
            this.k16.Text = "16.Koltuk";
            this.k16.UseVisualStyleBackColor = true;
            this.k16.Click += new System.EventHandler(this.k16_Click);
            // 
            // k17
            // 
            this.k17.Location = new System.Drawing.Point(451, 160);
            this.k17.Name = "k17";
            this.k17.Size = new System.Drawing.Size(75, 23);
            this.k17.TabIndex = 16;
            this.k17.Text = "17.Koltuk";
            this.k17.UseVisualStyleBackColor = true;
            this.k17.Click += new System.EventHandler(this.k17_Click);
            // 
            // k18
            // 
            this.k18.Location = new System.Drawing.Point(543, 160);
            this.k18.Name = "k18";
            this.k18.Size = new System.Drawing.Size(75, 23);
            this.k18.TabIndex = 17;
            this.k18.Text = "18.Koltuk";
            this.k18.UseVisualStyleBackColor = true;
            this.k18.Click += new System.EventHandler(this.k18_Click);
            // 
            // k19
            // 
            this.k19.Location = new System.Drawing.Point(451, 244);
            this.k19.Name = "k19";
            this.k19.Size = new System.Drawing.Size(75, 23);
            this.k19.TabIndex = 18;
            this.k19.Text = "19.Koltuk";
            this.k19.UseVisualStyleBackColor = true;
            this.k19.Click += new System.EventHandler(this.k19_Click);
            // 
            // k20
            // 
            this.k20.Location = new System.Drawing.Point(543, 244);
            this.k20.Name = "k20";
            this.k20.Size = new System.Drawing.Size(75, 23);
            this.k20.TabIndex = 19;
            this.k20.Text = "20.Koltuk";
            this.k20.UseVisualStyleBackColor = true;
            this.k20.Click += new System.EventHandler(this.k20_Click);
            // 
            // k21
            // 
            this.k21.Location = new System.Drawing.Point(451, 288);
            this.k21.Name = "k21";
            this.k21.Size = new System.Drawing.Size(75, 23);
            this.k21.TabIndex = 20;
            this.k21.Text = "21.Koltuk";
            this.k21.UseVisualStyleBackColor = true;
            this.k21.Click += new System.EventHandler(this.k21_Click);
            // 
            // k22
            // 
            this.k22.Location = new System.Drawing.Point(543, 288);
            this.k22.Name = "k22";
            this.k22.Size = new System.Drawing.Size(75, 23);
            this.k22.TabIndex = 21;
            this.k22.Text = "22.Koltuk";
            this.k22.UseVisualStyleBackColor = true;
            this.k22.Click += new System.EventHandler(this.k22_Click);
            // 
            // ksofor
            // 
            this.ksofor.Location = new System.Drawing.Point(265, 35);
            this.ksofor.Name = "ksofor";
            this.ksofor.Size = new System.Drawing.Size(75, 23);
            this.ksofor.TabIndex = 22;
            this.ksofor.Text = "Şöför";
            this.ksofor.UseVisualStyleBackColor = true;
            // 
            // kmuavin
            // 
            this.kmuavin.Location = new System.Drawing.Point(499, 35);
            this.kmuavin.Name = "kmuavin";
            this.kmuavin.Size = new System.Drawing.Size(75, 23);
            this.kmuavin.TabIndex = 23;
            this.kmuavin.Text = "Muavin";
            this.kmuavin.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(530, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 18);
            this.label1.TabIndex = 24;
            this.label1.Text = "KAPI";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(387, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 20);
            this.label2.TabIndex = 25;
            this.label2.Text = "koridor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.Location = new System.Drawing.Point(772, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 16);
            this.label3.TabIndex = 26;
            this.label3.Text = "Bileti Al";
            // 
            // bal
            // 
            this.bal.Location = new System.Drawing.Point(755, 61);
            this.bal.Name = "bal";
            this.bal.Size = new System.Drawing.Size(104, 64);
            this.bal.TabIndex = 27;
            this.bal.Text = "Bilet Al";
            this.bal.UseVisualStyleBackColor = true;
            // 
            // Koltuk
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1068, 416);
            this.Controls.Add(this.bal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.kmuavin);
            this.Controls.Add(this.ksofor);
            this.Controls.Add(this.k22);
            this.Controls.Add(this.k21);
            this.Controls.Add(this.k20);
            this.Controls.Add(this.k19);
            this.Controls.Add(this.k18);
            this.Controls.Add(this.k17);
            this.Controls.Add(this.k16);
            this.Controls.Add(this.k15);
            this.Controls.Add(this.k14);
            this.Controls.Add(this.k13);
            this.Controls.Add(this.k12);
            this.Controls.Add(this.k11);
            this.Controls.Add(this.k10);
            this.Controls.Add(this.k9);
            this.Controls.Add(this.k8);
            this.Controls.Add(this.k7);
            this.Controls.Add(this.k6);
            this.Controls.Add(this.k5);
            this.Controls.Add(this.k4);
            this.Controls.Add(this.k3);
            this.Controls.Add(this.k2);
            this.Controls.Add(this.K1);
            this.Name = "Koltuk";
            this.Text = "Koltuk";
            this.Load += new System.EventHandler(this.uyegiris_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button K1;
        private System.Windows.Forms.Button k2;
        private System.Windows.Forms.Button k3;
        private System.Windows.Forms.Button k4;
        private System.Windows.Forms.Button k5;
        private System.Windows.Forms.Button k6;
        private System.Windows.Forms.Button k7;
        private System.Windows.Forms.Button k8;
        private System.Windows.Forms.Button k9;
        private System.Windows.Forms.Button k10;
        private System.Windows.Forms.Button k11;
        private System.Windows.Forms.Button k12;
        private System.Windows.Forms.Button k13;
        private System.Windows.Forms.Button k14;
        private System.Windows.Forms.Button k15;
        private System.Windows.Forms.Button k16;
        private System.Windows.Forms.Button k17;
        private System.Windows.Forms.Button k18;
        private System.Windows.Forms.Button k19;
        private System.Windows.Forms.Button k20;
        private System.Windows.Forms.Button k21;
        private System.Windows.Forms.Button k22;
        private System.Windows.Forms.Button ksofor;
        private System.Windows.Forms.Button kmuavin;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bal;


    }
}