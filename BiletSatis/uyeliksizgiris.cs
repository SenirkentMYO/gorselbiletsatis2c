﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BiletSatis
{
    public partial class uyeliksizgiris : Form
    {
        public uyeliksizgiris()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (görselüyelikEntities1 gü = new görselüyelikEntities1())
            {
                var liste = from ü in gü.Sefer
                            where ü.BinisYeri == comboBox1.Text
                            select new
                            {
                                ü.BinisYeri,
                                ü.VarisYeri,
                                ü.KalkisSaati,
                                ü.SeferTarihi
                            };

                dgrid.DataSource = liste.ToList();
                var listee = from ü in gü.Sefer
                             where ü.VarisYeri == comboBox2.Text
                             select new
                             {
                                 ü.BinisYeri,
                                 ü.VarisYeri,
                                 ü.KalkisSaati,
                                 ü.SeferTarihi
                             };

                dgrid.DataSource = liste.ToList();

            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void uyeliksizgiris_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Koltuk kl = new Koltuk();
            kl.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogResult sonuc;
            sonuc = MessageBox.Show("Çıkmak İstediğinizden Eminmisiniz?", "Uyarı", MessageBoxButtons.OKCancel);
            {
                if (sonuc == DialogResult.OK)
                {
                    Application.Exit();
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Sorunlarıniz sl = new Sorunlarıniz();
            sl.Show();
        }
    }
}
